<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'ForumController@index');

Route::get('index-page','ForumController@index');
Route::post('save-data','ForumController@store');
Route::get('view-data','ForumController@view');
Route::get('comment-index/{id}','ForumController@commentIndex');
Route::post('save-comment/{id}','ForumController@update');
Route::get('view-answer/{id}','ForumController@answerView');
Route::post('count-like/{id}','ForumController@like');

// Route::get('locale/{locale}', function ($locale){
//     Session::put('locale', $locale);
//     return redirect()->back();
// });