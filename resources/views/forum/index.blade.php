@extends('layouts.app')
  @section('content')
      <div class="container">
        <center>
        <h2>Discussion Forum</h2>
        <hr>
        <form class="form-horizontal" action="/save-data" method="post">
          @csrf

            
          <div class="form-group">
            <div class="col-sm-8">
             <textarea name="topic" class="form-control" id="topic" placeholder="Enter Your Discussion Topic Here...."></textarea>
              @if($errors->first('topic'))
                  <p style="color:red;font-weight:bold; text-align: left;">{{ $errors->first('topic')}}</p>
              @endif
            </div>
          </div>
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn" style="background-color:#20B2AA;">Start New Discussion</button>
            </div>
          </div>
        </form>
        </center>
        <!-- <div class="content">
          <a href="/view-data"><b>Click Here</b></a> to View all disscussions
        </div> -->
      </div>
  @endsection

