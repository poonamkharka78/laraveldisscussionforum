@extends('layouts.app')
  @section('content')
      <div class="container">
        	<a href="/view-data" class="btn btn-success">Home</a>
        	   <hr>
             <table class="table">
            <thead>
              <tr>
                <th>Comment</th>
                <th>Total Likes</th>
                <th>Give Like</th>
                <th>Answered by</th>
              </tr>
            </thead>
            <tbody>
              @if($answers)
              @php
                $data=count($answers);
              @endphp
              
                @for ($i = 0; $i < $data; $i++)

                  <tr>
                    <td>{{ $answers[$i]['comment'] }}</td>
                    <td>@if(isset($count_likes[$i]['likes']))
                     {{ $count_likes [$i]['likes']}} @else 0 @endif</td>
                    <td>
                      <form action="{{url('count-like/'.$answers[$i]['id'])}}" method="post">
                            @csrf
                              <input type="submit" name="likes" class="btn btn-default btn-sm glyphicon glyphicon-thumbs-up" value="Like" />
                      </form>
                    </td>
                    <td><small>{{ $answers[$i]['user_name'] }}</small></td>
                  </tr>
                @endfor
              @endif 
            </tbody>
        </table>
      </div>
  @endsection

 
                    