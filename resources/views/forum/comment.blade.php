@extends('layouts.app')
  @section('content')
      <div class="container">
        <center>
        <h2>Discussion Forum</h2>
        <hr>
        <form class="form-horizontal" action="{{url('save-comment/'.$lists->id)}}" method="post">
          @csrf

            @if(Session::has('msgsuccess'))
              <div class="alert alert-success">
                <h5>{{ Session::get('msgsuccess')}}</h5>
              </div>
            @endif
            @if(Session::has('msgfail'))
              <div class="alert alert-danger">
                <h5>{{ Session::get('msgfail')}}</h5>
              </div>
            @endif

             <div class="form-group">
            <div class="col-sm-8">
                @if($lists) 
                 <h5>{{ $lists->topic }}</h5>
                @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-8">
             <textarea name="comment" class="form-control" id="comment" placeholder="Enter Your Comment Here...."></textarea>
             @if($errors->first('comment'))
             <p style="color: red;font-weight:bold; text-align: left;">{{$errors->first('comment')}}</p>
             @endif
            </div>
          </div>
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <a href="/view-data" class="btn btn-success">Home</a>
              <button type="submit" class="btn btn-success">Comment</button>
            </div>
          </div>
        </form>
        </center>
      </div>
  @endsection

