@extends('layouts.app')
  @section('content')

  <div class="container">
  	<a href="/index-page" class="btn btn-success">Start New Discussion</a>
  	<hr>
    @if(Session::has('success'))
              <div class="alert alert-success">
                <h5>{{ Session::get('success')}}</h5>
              </div>
            @endif
            @if(Session::has('fail'))
              <div class="alert alert-danger">
                <h5>{{ Session::get('fail')}}</h5>
              </div>
            @endif
        <table class="table">
            <thead>
              <tr>
                <th>Topic</th>
                <th>Posts</th>
                <th>Comments</th>
                <th>Give Comment</th>
                <th>Asked by</th>
              </tr>
            </thead>
            <tbody>
              @if($records)
              @php
                $data=count($records);
               @endphp
              
                @for ($i = 0; $i < $data; $i++)
                
                  <tr>
                    <td>{{ $records[$i]['topic'] }}</td>
                    <td>@if(isset($comment_like[$i]['total'])) {{ $comment_like[$i]['total']}} @else 0 @endif</td>
                    <td><a href="{{ url('view-answer/'.$records[$i]['id'])}}">View Posts</a></td>
                    <td><a href="{{url('comment-index/'.$records[$i]['id'])}}">Click</td>
                    <td><small>{{ $records[$i]['user_name'] }}</small></td>
                  </tr>
                @endfor
              @endif 
            </tbody>
           
        </table>
  </div>
  @endsection

  
                  