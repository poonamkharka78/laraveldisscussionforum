<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForumModel extends Model
{
    protected $table = "forum";

    public function commentModel(){
    	
    	return $this->hasOne('App\Models\CommentModel','discussion_id','id');
    }
}
