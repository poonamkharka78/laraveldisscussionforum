<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    public $table = "comment";

    public function ForumModel()
    {
        return $this->belongsTo('App\Models\ForumModel','discussion_id','id');
    }

    public function LikeDislikeModel(){
    	 return $this->hasOne('App\Models\CommentModel','comment_id','id');
    }
}
