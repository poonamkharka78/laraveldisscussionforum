<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikeDislikeModel extends Model
{
    protected $table = "likes_dislikes";

    public function CommentModel(){
    	 return $this->belongsTo('App\Models\CommentModel','comment_id','id');
    }
}
