<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\ForumModel;
use App\Models\CommentModel;
use App\Models\LikeDislikeModel;
use Auth;
use Session;
use DB;

class ForumController extends Controller
{
    public function index(){
    	return view('forum.index');
    }

    public function view(){
        $records = ForumModel::with('CommentModel')->get()->toArray();
        $comment_like = CommentModel::groupBy('discussion_id')->select('discussion_id', DB::raw('count(*) as total'))->get();

        return view('forum.forumList', compact('records','comment_like'));
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'topic' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/index-page')
                        ->withErrors($validator)
                        ->withInput();
        }

        $test = auth()->user()->name;
        $data = new ForumModel;
    	$data->topic = $request->topic;
    	$data->user_name = $test;
    	$save = $data->save();
    	if($save){
    		Session::flash('success','Your Topic of Disscusses Submitted');
    		return redirect('/view-data');
    	}
    	else{
    		Session::flash('fail','Your Topic of Disscusses Not Submitted');
    		return redirect('/view-data');
    	}
    }

    public function commentIndex($id){

    	$lists = ForumModel::find($id);
    	return view('forum.comment', compact('lists'));
    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ],[
            'comment.required' => "Please Enter Comment"]);

        if ($validator->fails()) {
            return redirect('comment-index/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

    	$l = ForumModel::find($id)->id;

    	$t = auth()->user()->name;

    	$insert = new CommentModel;
    	$insert->discussion_id = $l;
    	$insert->comment = $request->comment;
    	$insert->user_name= $t;
    	$in = $insert->save();
    	if($in){
    		Session::flash('msgsuccess','Your Comment Submitted');
    		return redirect('comment-index/'.$id);
    	}
    	else{
    		Session::flash('msgfail','Your Not Comment Submitted');
    		return redirect('comment-index/'.$id);
    	}


    }

    public function answerView($id){

       $answers = CommentModel::where('discussion_id',$id)->get();

        $count_likes = LikeDislikeModel::groupBy('likes')->select('likes', DB::raw('count(*) as likes'))
            ->where('comment_id',$id)
            ->get();


        return view('forum.answerList', compact('answers','count_likes'));
    }

    public function like(Request $request, $id){
       // $likes = LikeDislikeModel::where('comment_id',$id)->get('likes')->count();
       // dd($likes);
        $count = LikeDislikeModel::where('id', $id)->increment('likes');
        //dd($count);

        $commId = CommentModel::find($id);
        $idcommm = $commId->id;

        $myData = new LikeDislikeModel;
        $myData->comment_id = $idcommm;
        $myData->likes = $count = 1;
        //dd($myData);
        $store = $myData->save();
        if($store){
           return back();
        }
        else{
            return back();
        }


    }
}
